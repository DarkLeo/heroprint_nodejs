import { HttpClient }  from '@angular/common/http';
import { Injectable }  from '@angular/core';
import {
    ContainerApi,
    ShopItemImageApi,
    ShopItemImageInterface,
    ShopItemInterface,
}                      from '@app/sdk';
import { environment } from '../../environments/environment';

export type UploadModel = 'shopItem';

interface UploadInterface {
    id: {
        [key: string]: number;
    };
    container: string;
    api: ShopItemImageApi;
    file: File;
}

@Injectable({
    providedIn: 'root',
})
export class FileService {

    private loopbackContainerUrl: string = `${environment.loopback.baseURL}/${environment.loopback.apiVersion}/containers/`;
    private containers = {
        shopItemImage: 'shop-item-images',
    };

    constructor(
        private shopItemImageApi: ShopItemImageApi,
        private containerApi: ContainerApi,
        private httpClient: HttpClient,
    ) { }

    public uploadImage(id: number, model: UploadModel, file: File): Promise<ShopItemImageInterface> {
        let options: UploadInterface;
        switch ( model ) {
            case 'shopItem':
                options = {
                    container: this.containers.shopItemImage,
                    id: { shopItemId: id },
                    api: this.shopItemImageApi,
                    file,
                };
                break;
        }
        if ( options ) {
            return this.uploadFile(options);
        } else {
            return Promise.reject();
        }
    }

    public uploadFile({ id, container, api, file }: UploadInterface): Promise<ShopItemImageInterface> {
        return api.patchOrCreate(id)
            .toPromise<ShopItemImageInterface>()
            .then((shopItemImage) => {
                return this.uploadFileHttp(shopItemImage.filename, container, file)
                    .then((response) => {
                        return api.patchOrCreate({
                            id: shopItemImage.id,
                            filename: response.result.files.file[0].name,
                        }).toPromise<ShopItemImageInterface>()
                            .catch((err) => {
                                console.log('load error', err);
                                return api.deleteById(shopItemImage.id)
                                    .toPromise<ShopItemImageInterface>()
                                    .then((i) => Promise.reject());
                            });
                    });
            });
    }


    public getShopItemImages(shopItem: ShopItemInterface): ShopItemImageInterface[] {
        const containerName = this.containers.shopItemImage;
        if ( !shopItem.shopItemImages || shopItem.shopItemImages.length === 0 ) return [];
        return shopItem.shopItemImages.map((image: ShopItemImageInterface) => {
            image.url = `${this.loopbackContainerUrl}/${containerName}/download/${image.filename}`;
            return image;
        });
    }

    public async deleteShopItemImages(shopItemImages: ShopItemImageInterface[]) {
        return Promise.all(
            shopItemImages.map((shopItemImage) => {
                return this.shopItemImageApi.deleteById(shopItemImage.id)
                    .toPromise()
                    .then(() => this.containerApi.removeFile(this.containers.shopItemImage, shopItemImage.filename).toPromise());
            }),
        );
    }

    private uploadFileHttp(fileName: string, containerName: string, file: File): Promise<any> {
        const fd = new FormData();
        fd.append('file', file);
        const url = `${this.loopbackContainerUrl}/${containerName}/upload?filename=${fileName}`;
        return this.httpClient.post(url, fd).toPromise();
    }
}
