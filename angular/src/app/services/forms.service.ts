import { Injectable }        from '@angular/core';
import {
    FormControl,
    FormGroup,
}                            from '@angular/forms';
import { ShopItemInterface } from '@app/sdk';

@Injectable({
    providedIn: 'root',
})
export class FormsService {

    constructor() { }

    public generateForm(
        shopItem: ShopItemInterface,
        { disabled = [], hide = [], date = [], textarea = [], checkbox = [], number = [], order = undefined },
    ): FormGroup {
        const form: FormGroup = new FormGroup({});

        for ( const control in shopItem ) {
            if ( shopItem.hasOwnProperty(control) ) {

                // set control
                if ( date.find((d) => d === control) ) {
                    form.addControl(control, new FormControl(new Date(shopItem[control])));
                } else {
                    form.addControl(control, new FormControl(shopItem[control]));
                }

                // set hide
                form.get(control)['hide'] = !!hide.find((h) => h === control);

                // set type
                form.get(control)['type'] = undefined;
                if ( date.find((d) => d === control) ) {
                    form.get(control)['type'] = 'date';
                } else if ( textarea.find((t) => t === control) ) {
                    form.get(control)['type'] = 'textarea';
                } else if ( checkbox.find((c) => c === control) ) {
                    form.get(control)['type'] = 'checkbox';
                } else if ( number.find((n) => n === control) ) {
                    form.get(control)['type'] = 'number';
                } else {
                    form.get(control)['type'] = 'text';
                }

                // set disabled
                if ( disabled.find((d) => d === control) ) {
                    form.get(control).disable();
                }
            }
        }

        form['order'] = order || Object.keys(form.controls);
        return form;
    }
}
