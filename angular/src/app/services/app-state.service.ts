import { Injectable }      from '@angular/core';
import {
    NavigationEnd,
    Router,
}                          from '@angular/router';
import { MenuItem }        from 'primeng';
import { BehaviorSubject } from 'rxjs';
import { filter }          from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class AppStateService {
    public routerState: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
    public breadcrumbs: BehaviorSubject<MenuItem[]> = new BehaviorSubject<MenuItem[]>([]);

    constructor(
        private router: Router,
    ) {
        this.initRouterState();
    }

    private initRouterState(): void {
        this.router.events
            .pipe(filter((e): e is NavigationEnd => e instanceof NavigationEnd))
            .subscribe((e) => {
                const nav: string[] = e.url.split('/').filter((f) => !!f);
                this.routerState.next(nav);

                let acc: string[] = [];
                this.breadcrumbs.next(
                    nav.map((n, i, arr): MenuItem => {
                        acc.push(n);
                        return {
                            label: n,
                            routerLink: '/' + acc.join('/')
                        }
                    })
                )
            });

        this.breadcrumbs.subscribe((b) => console.log(b))
    }

}
