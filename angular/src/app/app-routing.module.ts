import { ModuleWithProviders }     from '@angular/core';
import {
    RouterModule,
    Routes,
}                                  from '@angular/router';
import { AdminListComponent }      from '@components/admin-list/admin-list.component';
import { AdminComponent }          from '@components/admin-panel/admin.component';
import { LoginComponent }          from '@components/login/login.component';
import { ShopItemEditorComponent } from '@components/shop-item-editor/shop-item-editor.component';
import { SignInComponent }         from '@components/sign-in/sign-in.component';
import { TestComponent }           from '@components/test/test.component';
import { ChartsDemoComponent }     from './demo/view/chartsdemo.component';
import { DashboardDemoComponent }  from './demo/view/dashboarddemo.component';
import { DataDemoComponent }       from './demo/view/datademo.component';
import { DocumentationComponent }  from './demo/view/documentation.component';
import { EmptyDemoComponent }      from './demo/view/emptydemo.component';
import { FileDemoComponent }       from './demo/view/filedemo.component';
import { FormsDemoComponent }      from './demo/view/formsdemo.component';
import { MenusDemoComponent }      from './demo/view/menusdemo.component';
import { MessagesDemoComponent }  from './demo/view/messagesdemo.component';
import { MiscDemoComponent }      from './demo/view/miscdemo.component';
import { OverlaysDemoComponent }  from './demo/view/overlaysdemo.component';
import { PanelsDemoComponent }    from './demo/view/panelsdemo.component';
import { SampleDemoComponent }    from './demo/view/sampledemo.component';

export const routes: Routes = [
    { path: '', component: TestComponent },
    {
        path: 'admin',
        component: AdminComponent,
        children: [
            {
                path: '',
                component: AdminListComponent,
            },
            {
                path: 'editor',
                children: [
                    {
                        path: 'shop-item',
                        component: ShopItemEditorComponent,
                    },
                    {
                        path: 'shop-item/:id',
                        component: ShopItemEditorComponent,
                    }
                ]
            },
            {
                path: '**',
                component: AdminListComponent,
            },
        ],
    },
    { path: 'login', component: LoginComponent },
    { path: 'sign-in', component: SignInComponent },
    { path: 'dashboard', component: DashboardDemoComponent },
    { path: 'components/sample', component: SampleDemoComponent },
    { path: 'components/forms', component: FormsDemoComponent },
    { path: 'components/data', component: DataDemoComponent },
    { path: 'components/panels', component: PanelsDemoComponent },
    { path: 'components/overlays', component: OverlaysDemoComponent },
    { path: 'components/menus', component: MenusDemoComponent },
    { path: 'components/messages', component: MessagesDemoComponent },
    { path: 'components/misc', component: MiscDemoComponent },
    { path: 'pages/empty', component: EmptyDemoComponent },
    { path: 'components/charts', component: ChartsDemoComponent },
    { path: 'components/file', component: FileDemoComponent },
    { path: 'documentation', component: DocumentationComponent },
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' });
