import { Injectable }      from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class AppStyleService {

    private _wrapperPadding: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

    constructor() { }

    public get padding(): boolean {
        return this._wrapperPadding.getValue();
    }

    public set padding(value: boolean) {
        this._wrapperPadding.next(value);
    }

    public get asyncPadding(): BehaviorSubject<boolean> {
        return this._wrapperPadding;
    }
}
