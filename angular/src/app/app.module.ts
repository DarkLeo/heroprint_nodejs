import {
    HashLocationStrategy,
    LocationStrategy,
}                                  from '@angular/common';
import { HttpClientModule }        from '@angular/common/http';
import { NgModule }                from '@angular/core';
import {
    FormsModule,
    ReactiveFormsModule,
} from '@angular/forms';
import { BrowserModule }           from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppFooterComponent }      from '@components/footer/app.footer.component';
import { AppMenuComponent }        from '@components/menu/app.menu.component';
import { MenuService }          from '@components/menu/app.menu.service';
import { AppMenuitemComponent } from '@components/menu-item/app.menuitem.component';
import { TestComponent }        from '@components/test/test.component';
import { AppTopBarComponent }      from '@components/topbar/app.topbar.component';

import { AccordionModule }         from 'primeng/accordion';
import { AutoCompleteModule }      from 'primeng/autocomplete';
import { BreadcrumbModule }        from 'primeng/breadcrumb';
import { ButtonModule }            from 'primeng/button';
import { CalendarModule }          from 'primeng/calendar';
import { CardModule }              from 'primeng/card';
import { CarouselModule }          from 'primeng/carousel';
import { ChartModule }             from 'primeng/chart';
import { CheckboxModule }          from 'primeng/checkbox';
import { ChipsModule }             from 'primeng/chips';
import { CodeHighlighterModule }   from 'primeng/codehighlighter';
import { ColorPickerModule }       from 'primeng/colorpicker';
import { ConfirmDialogModule }     from 'primeng/confirmdialog';
import { ContextMenuModule }       from 'primeng/contextmenu';
import { DataViewModule }          from 'primeng/dataview';
import { DialogModule }            from 'primeng/dialog';
import { DropdownModule }          from 'primeng/dropdown';
import { EditorModule }            from 'primeng/editor';
import { FieldsetModule }          from 'primeng/fieldset';
import { FileUploadModule }        from 'primeng/fileupload';
import { FullCalendarModule }      from 'primeng/fullcalendar';
import { GalleriaModule }          from 'primeng/galleria';
import { InplaceModule }           from 'primeng/inplace';
import { InputMaskModule }         from 'primeng/inputmask';
import { InputSwitchModule }       from 'primeng/inputswitch';
import { InputTextModule }         from 'primeng/inputtext';
import { InputTextareaModule }     from 'primeng/inputtextarea';
import { LightboxModule }          from 'primeng/lightbox';
import { ListboxModule }           from 'primeng/listbox';
import { MegaMenuModule }          from 'primeng/megamenu';
import { MenuModule }              from 'primeng/menu';
import { MenubarModule }           from 'primeng/menubar';
import { MessageModule }           from 'primeng/message';
import { MessagesModule }          from 'primeng/messages';
import { MultiSelectModule }       from 'primeng/multiselect';
import { OrderListModule }         from 'primeng/orderlist';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { OverlayPanelModule }      from 'primeng/overlaypanel';
import { PaginatorModule }         from 'primeng/paginator';
import { PanelModule }             from 'primeng/panel';
import { PanelMenuModule }         from 'primeng/panelmenu';
import { PasswordModule }          from 'primeng/password';
import { PickListModule }          from 'primeng/picklist';
import { ProgressBarModule }       from 'primeng/progressbar';
import { RadioButtonModule }       from 'primeng/radiobutton';
import { RatingModule }            from 'primeng/rating';
import { ScrollPanelModule }       from 'primeng/scrollpanel';
import { SelectButtonModule }      from 'primeng/selectbutton';
import { SlideMenuModule }         from 'primeng/slidemenu';
import { SliderModule }            from 'primeng/slider';
import { SpinnerModule }           from 'primeng/spinner';
import { SplitButtonModule }       from 'primeng/splitbutton';
import { StepsModule }             from 'primeng/steps';
import { TableModule }             from 'primeng/table';
import { TabMenuModule }           from 'primeng/tabmenu';
import { TabViewModule }           from 'primeng/tabview';
import { TerminalModule }          from 'primeng/terminal';
import { TieredMenuModule }        from 'primeng/tieredmenu';
import { ToastModule }             from 'primeng/toast';
import { ToggleButtonModule }      from 'primeng/togglebutton';
import { ToolbarModule }           from 'primeng/toolbar';
import { TooltipModule }           from 'primeng/tooltip';
import { TreeModule }              from 'primeng/tree';
import { TreeTableModule }         from 'primeng/treetable';
import { VirtualScrollerModule }   from 'primeng/virtualscroller';
import { environment }             from '../environments/environment';
import { AppRoutingModule }        from './app-routing.module';

import { AppComponent } from './app.component';

import { CarService }             from './demo/service/carservice';
import { CountryService }         from './demo/service/countryservice';
import { EventService }           from './demo/service/eventservice';
import { NodeService }            from './demo/service/nodeservice';
import { ChartsDemoComponent }    from './demo/view/chartsdemo.component';
import { DashboardDemoComponent } from './demo/view/dashboarddemo.component';
import { DataDemoComponent }      from './demo/view/datademo.component';
import { DocumentationComponent } from './demo/view/documentation.component';
import { EmptyDemoComponent }     from './demo/view/emptydemo.component';
import { FileDemoComponent }      from './demo/view/filedemo.component';
import { FormsDemoComponent }     from './demo/view/formsdemo.component';
import { MenusDemoComponent }     from './demo/view/menusdemo.component';
import { MessagesDemoComponent }  from './demo/view/messagesdemo.component';
import { MiscDemoComponent }      from './demo/view/miscdemo.component';
import { OverlaysDemoComponent }  from './demo/view/overlaysdemo.component';
import { PanelsDemoComponent }    from './demo/view/panelsdemo.component';
import { SampleDemoComponent }    from './demo/view/sampledemo.component';
import {
    LoopBackConfig,
    SDKBrowserModule,
}                                 from './sdk';
import { LoginComponent } from './components/login/login.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { UploadFileComponent } from './components/upload-file/upload-file.component';
import { ShopItemEditorComponent } from './components/shop-item-editor/shop-item-editor.component';
import { UFormComponent } from './components/u-form/u-form.component';
import { AdminComponent } from '@components/admin-panel/admin.component';
import { AdminListComponent } from './components/admin-list/admin-list.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        SDKBrowserModule.forRoot(),
        HttpClientModule,
        BrowserAnimationsModule,
        AccordionModule,
        AutoCompleteModule,
        BreadcrumbModule,
        ButtonModule,
        CalendarModule,
        CardModule,
        CarouselModule,
        ChartModule,
        CheckboxModule,
        ChipsModule,
        CodeHighlighterModule,
        ConfirmDialogModule,
        ColorPickerModule,
        ContextMenuModule,
        DataViewModule,
        DialogModule,
        DropdownModule,
        EditorModule,
        FieldsetModule,
        FileUploadModule,
        FullCalendarModule,
        GalleriaModule,
        InplaceModule,
        InputMaskModule,
        InputSwitchModule,
        InputTextModule,
        InputTextareaModule,
        LightboxModule,
        ListboxModule,
        MegaMenuModule,
        MenuModule,
        MenubarModule,
        MessageModule,
        MessagesModule,
        MultiSelectModule,
        OrderListModule,
        OrganizationChartModule,
        OverlayPanelModule,
        PaginatorModule,
        PanelModule,
        PanelMenuModule,
        PasswordModule,
        PickListModule,
        ProgressBarModule,
        RadioButtonModule,
        RatingModule,
        ScrollPanelModule,
        SelectButtonModule,
        SlideMenuModule,
        SliderModule,
        SpinnerModule,
        SplitButtonModule,
        StepsModule,
        TableModule,
        TabMenuModule,
        TabViewModule,
        TerminalModule,
        TieredMenuModule,
        ToastModule,
        ToggleButtonModule,
        ToolbarModule,
        TooltipModule,
        TreeModule,
        TreeTableModule,
        VirtualScrollerModule,
        ReactiveFormsModule,
    ],
    declarations: [
	    AppComponent,
	    AppMenuComponent,
	    AppMenuitemComponent,
	    AppTopBarComponent,
	    AppFooterComponent,
	    DashboardDemoComponent,
	    SampleDemoComponent,
	    FormsDemoComponent,
	    DataDemoComponent,
	    PanelsDemoComponent,
	    OverlaysDemoComponent,
	    MenusDemoComponent,
	    MessagesDemoComponent,
	    MessagesDemoComponent,
	    MiscDemoComponent,
	    ChartsDemoComponent,
	    EmptyDemoComponent,
	    FileDemoComponent,
	    DocumentationComponent,
	    TestComponent,
	    LoginComponent,
	    SignInComponent,
	    UploadFileComponent,
	    ShopItemEditorComponent,
	    UFormComponent,
	    AdminComponent,
	    AdminListComponent,
    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        CarService, CountryService, EventService, NodeService, MenuService,
    ],
    bootstrap: [ AppComponent ],
})
export class AppModule {
    constructor() {
        LoopBackConfig.setBaseURL(environment.loopback.baseURL);
        LoopBackConfig.setApiVersion(environment.loopback.apiVersion);
    }
}
