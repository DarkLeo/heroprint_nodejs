import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent }       from './app.component';
import { AppTopBarComponent } from './components/topbar/app.topbar.component';
import { AppMenuComponent }   from './components/menu/app.menu.component';
import { AppFooterComponent } from './app.footer.component';
import { ScrollPanelModule }  from 'primeng/scrollpanel';
import { MenuService }        from './components/menu/app.menu.service';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                ScrollPanelModule,
                RouterTestingModule
            ],
            declarations: [
                AppTopBarComponent,
                AppMenuComponent,
                AppFooterComponent,
                AppComponent
            ],
            providers: [MenuService]
        }).compileComponents();
    }));

    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

});
