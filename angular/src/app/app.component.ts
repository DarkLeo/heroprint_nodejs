import {
    Component,
    OnInit,
}                           from '@angular/core';
import { AppStyleService }  from '@app/app-style.service';
import { BaseUserApi }      from '@app/sdk';
import { AppStateService }  from '@app/services/app-state.service';
import { AppFooterService } from '@components/footer/app.footer.service';
import { MenuService }      from '@components/menu/app.menu.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: [ './app.component.scss' ],
})
export class AppComponent implements OnInit {

    public menuMode = 'static';
    public topbarMenuActive: boolean;
    public overlayMenuActive: boolean;
    public staticMenuDesktopInactive: boolean;
    public staticMenuMobileActive: boolean;
    public lightMenu = false;
    public topbarColor = 'layout-topbar-bluegrey';
    public lightTopbar = false;
    public menuHoverActive: boolean;

    layoutMenuScroller: HTMLDivElement;

    private menuClick: boolean;
    private topbarItemClick: boolean;
    private resetMenu: boolean;

    constructor(
        private menuService: MenuService,
        private baseUserApi: BaseUserApi,
        public appFooterService: AppFooterService,
        public appStyleService: AppStyleService,
        public appStateService: AppStateService,
    ) { }

    public ngOnInit(): void {
        this.baseUserApi.getCurrent()
            .subscribe((user: BaseUserApi) => {
                console.log(user);
            }, (error => console.log(error)));
    }

    public onLayoutClick() {
        if ( !this.topbarItemClick ) {
            this.topbarMenuActive = false;
        }

        if ( !this.menuClick ) {
            if ( this.isHorizontal() || this.isSlim() ) {
                this.menuService.reset();
            }

            if ( this.overlayMenuActive || this.staticMenuMobileActive ) {
                this.hideOverlayMenu();
            }

            this.menuHoverActive = false;
        }

        this.topbarItemClick = false;
        this.menuClick = false;
    }

    public onMenuButtonClick(event) {
        this.menuClick = true;
        this.topbarMenuActive = false;

        if ( this.isOverlay() ) {
            this.overlayMenuActive = !this.overlayMenuActive;
        }
        if ( this.isDesktop() ) {
            this.staticMenuDesktopInactive = !this.staticMenuDesktopInactive;
        } else {
            this.staticMenuMobileActive = !this.staticMenuMobileActive;
        }

        event.preventDefault();
    }

    public onMenuClick($event) {
        this.menuClick = true;
        this.resetMenu = false;
    }

    public onTopbarMenuButtonClick(event) {
        this.topbarItemClick = true;
        this.topbarMenuActive = !this.topbarMenuActive;

        this.hideOverlayMenu();

        event.preventDefault();
    }

    public onTopbarSubItemClick(event) {
        event.preventDefault();
    }

    public isHorizontal() {
        return this.menuMode === 'horizontal';
    }

    isStatic() {
        return this.menuMode === 'static';
    }

    isMobile() {
        return window.innerWidth < 1025;
    }

    public isDesktop() {
        return window.innerWidth > 1024;
    }

    isTablet() {
        const width = window.innerWidth;
        return width <= 1024 && width > 640;
    }

    public changeMenuMode(menuMode: string) {
        this.menuMode = menuMode;
        this.staticMenuDesktopInactive = false;
        this.overlayMenuActive = false;
    }

    public isSlim() {
        return this.menuMode === 'slim';
    }

    private isOverlay() {
        return this.menuMode === 'overlay';
    }

    private hideOverlayMenu() {
        this.overlayMenuActive = false;
        this.staticMenuMobileActive = false;
    }
}
