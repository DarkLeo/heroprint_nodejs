import {
    Component,
    OnDestroy,
    OnInit,
}                          from '@angular/core';
import { AppStateService } from '@app/services/app-state.service';
import { MenuItem }        from 'primeng';
import {
    BehaviorSubject,
    Subscription,
}                          from 'rxjs';

@Component({
    selector: 'app-admin-panel',
    templateUrl: './admin.component.html',
    styleUrls: [ './admin.component.scss' ],
})
export class AdminComponent implements OnInit, OnDestroy {

    breadcrumbs: BehaviorSubject<MenuItem[]> = new BehaviorSubject<MenuItem[]>([]);

    private s: Subscription[] = [];

    constructor(
        private appStateService: AppStateService,
    ) { }

    public ngOnInit(): void {
        const s = this.appStateService.breadcrumbs.subscribe((stateBC) => {
            const notInclude: string[] = [ 'editor' ];
            this.breadcrumbs.next(
                stateBC
                    .filter((bc) => {
                        return !notInclude.find((a) => a === bc.label);
                    })
            );
        });
        this.s.push(s);
    }

    public ngOnDestroy(): void {
        if ( this.s.length > 0 ) this.s.forEach((s) => s.unsubscribe());
    }

}
