import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopItemEditorComponent } from './shop-item-editor.component';

describe('ShopItemEditorComponent', () => {
  let component: ShopItemEditorComponent;
  let fixture: ComponentFixture<ShopItemEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopItemEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopItemEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
