import {
    Component,
    Input,
    OnInit,
}                          from '@angular/core';
import { FormGroup }       from '@angular/forms';
import {
    ActivatedRoute,
    Router,
}                          from '@angular/router';
import {
    ShopItem,
    ShopItemApi,
    ShopItemImageApi,
    ShopItemImageInterface,
    ShopItemInterface,
}                          from '@app/sdk';
import { AppStateService } from '@app/services/app-state.service';
import { FileService }     from '@app/services/file.service';
import { FormsService }    from '@app/services/forms.service';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-shop-item-editor',
    templateUrl: './shop-item-editor.component.html',
    styleUrls: [ './shop-item-editor.component.scss' ],
})
export class ShopItemEditorComponent implements OnInit {

    @Input() public id: number = Number(this.activatedRoute.snapshot.params['id']) || null;
    public shopItem: BehaviorSubject<ShopItemInterface> = new BehaviorSubject<ShopItemInterface>(undefined);
    public shopItemForm: FormGroup;
    public shopItemImages: ShopItemImageInterface[] = [];

    constructor(
        private shopItemApi: ShopItemApi,
        private shopItemImageApi: ShopItemImageApi,
        private formsService: FormsService,
        private activatedRoute: ActivatedRoute,
        private fileService: FileService,
        private router: Router,
        private appStateService: AppStateService
    ) { }

    public ngOnInit(): void {
        this.initShopItemSubscription();

        if ( this.id ) {
           this.getShopItem();
        } else {
            const shopItem = new ShopItem();
            shopItem.created = new Date();
            shopItem.updated = new Date();
            shopItem.id = undefined;
            this.shopItem.next(shopItem);
        }
    }

    private initShopItemSubscription() {
        this.shopItem.subscribe((shopItem) => {
            const disabled: string[] = [ 'id', 'created' ];
            const hide: string[] = [ 'id', 'shopCategories', 'shopItemImages', 'images' ];
            const date: string[] = [ 'created', 'updated' ];
            const textarea: string[] = [ 'desc', 'descShort' ];
            const checkbox: string[] = [ 'isActive' ];
            const number: string[] = [ 'price' ];
            const order: string[] = [
                'id',
                'name',
                'nameLat',
                'price',
                'desc',
                'descShort',
                'isActive',
                'created',
                'updated',
            ];

            this.shopItemForm = this.formsService.generateForm(shopItem, {
                disabled,
                hide,
                date,
                textarea,
                checkbox,
                number,
                order,
            });

            if ( !shopItem ) return;
            this.shopItemImages = this.fileService.getShopItemImages(shopItem);
        });
    }

    public async submit(): Promise<void> {
        const shopItem: ShopItemInterface = this.shopItemForm.getRawValue();
        const labels: string[] = [
            'name',
            'nameLat',
            'desc',
            'descShort',
            'price',
            'created',
            'updated',
            'isActive',
            'id',
        ];
        Object.keys(shopItem).forEach((key) => {
            if ( !labels.find((label) => label === key) ) {
                delete shopItem[key];
            }
        });
        return this.shopItemApi.patchOrCreate(shopItem)
            .toPromise<ShopItemInterface>()
            .then((shopItem) => {
                this.id = shopItem.id;
                this.shopItem.next(shopItem);

                const bc = this.appStateService.breadcrumbs.getValue();
                if (bc[bc.length - 1].routerLink !== String(shopItem.id)) {
                    this.router.navigateByUrl('/admin/editor/shop-item/' + shopItem.id);
                }
                return Promise.resolve();
            });
    }

    public async getShopItem(): Promise<ShopItemInterface> {
        return this.shopItemApi.findById<ShopItemInterface>(this.id, { include: 'shopItemImages' }).toPromise()
            .then((shopItem) => {
                this.id = shopItem.id;
                this.shopItem.next(shopItem);
                return Promise.resolve(shopItem);
            });
    }

    public async deleteImage(shopItemImage: ShopItemImageInterface) {
        return this.fileService.deleteShopItemImages([ shopItemImage])
            .then(() => this.getShopItem());
    }

    public deleteShopItem() {
        Promise.all([
            this.shopItemApi.deleteById(this.id).toPromise(),
            this.fileService.deleteShopItemImages(this.shopItemImages)
        ])
            .then(() => {
                const bc = this.appStateService.breadcrumbs.getValue();
                this.router.navigateByUrl('/admin');
            })
    }

}
