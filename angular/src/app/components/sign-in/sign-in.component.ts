import {
    Component,
    OnInit,
}                           from '@angular/core';
import {
    FormControl,
    FormGroup,
}                           from '@angular/forms';
import { AppStyleService }  from '@app/app-style.service';
import {
    BaseUserApi,
    LoopBackAuth,
    SDKToken,
}                           from '@app/sdk';
import { AppFooterService } from '@components/footer/app.footer.service';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: [ './sign-in.component.scss' ],
})
export class SignInComponent implements OnInit {

    public form: FormGroup = new FormGroup({
        username: new FormControl(),
        email: new FormControl(),
        password: new FormControl(),
        passwordConfirm: new FormControl(),
    });
    private _footerPreviousStatus: boolean;
    private _paddingPreviousStatus: boolean;

    constructor(
        private appFooterService: AppFooterService,
        private appStyleService: AppStyleService,
        private baseUserApi: BaseUserApi,
        private authService: LoopBackAuth,
    ) { }

    public ngOnInit(): void {
        setTimeout(() => {
            this._footerPreviousStatus = this.appFooterService.isVisible.getValue();
            this.appFooterService.isVisible.next(false);

            this._paddingPreviousStatus = this.appStyleService.padding;
            this.appStyleService.padding = false;
        });
    }

    public ngOnDestroy(): void {
        this.appFooterService.isVisible.next(this._footerPreviousStatus);
        this.appStyleService.padding = this._paddingPreviousStatus;
    }

    public submit(): void {
        this.baseUserApi.create(this.form.value)
            .toPromise()
            .then(() => {
                return this.baseUserApi.login({
                    email: this.form.value.email,
                    password: this.form.value.password,
                })
                    .toPromise();
            })
            .then((token: SDKToken) => {
                console.log(token);
                if ( token.id && token.userId ) {
                    this.authService.setToken(token);
                    this.authService.setRememberMe(true);
                    this.authService.save();
                }
            });
    }
}
