import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
}                          from '@angular/core';
import {
    FileService,
    UploadModel,
}                          from '@app/services/file.service';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-upload-file',
    templateUrl: './upload-file.component.html',
    styleUrls: [ './upload-file.component.scss' ],
})
export class UploadFileComponent implements OnInit {

    public imageLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    @Input() public multiple: boolean = false;
    @Input() public maxFileSize: number = 50000000;
    @Input() public disabled: boolean = false;
    @Input() private id: number;
    @Input() private uploadModel: UploadModel;
    @Output() private uploaded: EventEmitter<void> = new EventEmitter<void>();
    @Output() private error: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        private fileService: FileService,
    ) { }

    public ngOnInit(): void {}

    public upload(event) {
        if (!this.uploadModel) {
            console.error('Check uploadModel!');
            return;
        } else if (!this.id) {
            console.error('Check id!');
            return;
        }

        this.imageLoading.next(true);
        Promise.all(
            event.files.map((file) => {
                return this.fileService.uploadImage(this.id, this.uploadModel, file);
            }),
        )
            .then((c) => {
                this.imageLoading.next(false);
                this.uploaded.emit();
            })
            .catch(() => {
                this.imageLoading.next(false);
                this.error.emit();
            });
    }

}
