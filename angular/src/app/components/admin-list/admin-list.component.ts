import {
    Component,
    OnInit,
}                   from '@angular/core';
import {
    ShopItemApi,
    ShopItemInterface,
}                   from '@app/sdk';
import { MenuItem } from 'primeng';

@Component({
    selector: 'app-admin-list',
    templateUrl: './admin-list.component.html',
    styleUrls: [ './admin-list.component.scss' ],
})
export class AdminListComponent implements OnInit {

    public menuItems: MenuItem[] = [
        {
            label: 'shop',
            items: [
                {
                    label: 'shop-items',
                    command: () => this.getShopItems(),
                },
            ],
        },
    ];

    public tableItems: any[] = [];
    public tableCols: any[] = [];

    constructor(
        private shopItemApi: ShopItemApi,
    ) { }

    public ngOnInit(): void {
        this.tableCols = [ 'id', 'name' ].map((label) => {return { label };});
    }

    private getShopItems(): void {
        this.shopItemApi.find<ShopItemInterface>()
            .toPromise()
            .then((shopItems) => {
                shopItems.unshift({
                    created: undefined,
                    isActive: false,
                    nameLat: '',
                    price: 0,
                    updated: undefined,
                    name: 'Новый товар',
                    id: null,
                });
                shopItems.forEach((item) => item['routerLink'] = '/admin/editor/shop-item/');
                this.tableItems = shopItems;
            });
    }

}
