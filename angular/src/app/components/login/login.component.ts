import {
    AfterViewInit,
    Component,
    OnDestroy,
    OnInit,
}                           from '@angular/core';
import {
    FormControl,
    FormGroup,
}                           from '@angular/forms';
import { AppStyleService }  from '@app/app-style.service';
import {
    BaseUserApi,
    LoopBackAuth,
    SDKToken,
}                           from '@app/sdk';
import { AppFooterService } from '@components/footer/app.footer.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: [ './login.component.scss' ],
})
export class LoginComponent implements OnInit, OnDestroy, AfterViewInit {

    public form: FormGroup = new FormGroup({
        email: new FormControl(),
        password: new FormControl(),
    });
    private _footerPreviousStatus: boolean;
    private _paddingPreviousStatus: boolean;

    constructor(
        private appFooterService: AppFooterService,
        private appStyleService: AppStyleService,
        private baseUserApi: BaseUserApi,
        private authService: LoopBackAuth,
    ) { }

    public ngOnInit(): void {

    }

    public ngAfterViewInit(): void {
        setTimeout(() => {
            this._footerPreviousStatus = this.appFooterService.isVisible.getValue();
            this.appFooterService.isVisible.next(false);

            this._paddingPreviousStatus = this.appStyleService.padding;
            this.appStyleService.padding = false;
        });
    }

    public ngOnDestroy(): void {
        this.appFooterService.isVisible.next(this._footerPreviousStatus);
        this.appStyleService.padding = this._paddingPreviousStatus;
    }

    public submit(): void {
        this.baseUserApi.login({
            email: this.form.get('email').value,
            password: this.form.get('password').value,
        })
            .toPromise()
            .then((token: SDKToken) => {
                if ( token.id && token.userId ) {
                    this.authService.setToken(token);
                    this.authService.setRememberMe(true);
                    this.authService.save();
                }
            });
    }

}
