import { Injectable }      from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class AppFooterService {

    public isVisible: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

    constructor() { }
}
