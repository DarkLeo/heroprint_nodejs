import { TestBed } from '@angular/core/testing';

import { App.FooterService } from './app.footer.service';

describe('App.FooterService', () => {
  let service: App.FooterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(App.FooterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
