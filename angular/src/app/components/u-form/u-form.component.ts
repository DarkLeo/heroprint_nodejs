import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
}                    from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-u-form',
    templateUrl: './u-form.component.html',
    styleUrls: [ './u-form.component.css' ],
})
export class UFormComponent implements OnInit {

    @Input() public formGroup: FormGroup;
    @Input() public label: string;
    @Output() public submit: EventEmitter<void> = new EventEmitter<void>();

    constructor() { }

    public ngOnInit(): void {}
}
