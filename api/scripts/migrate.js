const server = require('../server/server');
const ds = server.dataSources['PostgresDB'];
const lbTables = ['ShopCart'];
ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' - lbTables - '] created in ', ds.adapter.name);
  ds.disconnect();
});
